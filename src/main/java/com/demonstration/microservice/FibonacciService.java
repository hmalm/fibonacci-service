package com.demonstration.microservice;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@SpringBootApplication
@RestController
public class FibonacciService {
    private static final Log log = LogFactory.getLog(FibonacciService.class);
    private static final LinkedList<Long> cache = new LinkedList<>();

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/fibonacci/{n}",
            produces = APPLICATION_JSON_VALUE)
    public List<Long> fibonacciUpTo(@PathVariable Integer n) {
        if (cache.size() > n) { // doesn't ever shrink in size only grows in slices, so only need to lock on growth.
            return cache.subList(0, n + 1);
        }
        synchronized(cache) {  // could also use a ConcurrentLinkedQueue, but just to demonstrate.
            cache.addAll(fibonacciStack(n, new LinkedList<>()).subList(cache.size(), n + 1));
            return cache;
        }
    }

    public static long fibonacci(int i) {
        if (i == 0)
            return 0;
        if (i <= 2)
            return 1;
        return fibonacci(i - 1) + fibonacci(i - 2);
    }

    LinkedList<Long> fibonacciStack(Integer n, LinkedList<Long> stack) {
        if (n == 0) {
            stack.addFirst(0L);
            return stack;
        }
        stack.addFirst(fibonacci(n));
        return fibonacciStack(n - 1, stack);
    }

    public static void main(String[] args) {
        SpringApplication.run(FibonacciService.class, args);
    }

}
