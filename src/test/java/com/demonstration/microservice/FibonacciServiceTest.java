package com.demonstration.microservice;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class FibonacciServiceTest {

    @Test
    public void testFibonacci() {
        List<Long> expected = Arrays.asList(0L, 1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L, 89L, 144L);
        for(int i = 0; i < expected.size(); i++) {
            assertTrue(FibonacciService.fibonacci(i) == expected.get(i));
        }
    }

    @Test
    public void testFibonacciStack() {
        List<Long> expected = Arrays.asList(0L, 1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L, 89L, 144L);
        FibonacciService fs = new FibonacciService();
        LinkedList<Long> result = fs.fibonacciStack(12, new LinkedList<>());
        assertEquals(result, expected);
    }
}