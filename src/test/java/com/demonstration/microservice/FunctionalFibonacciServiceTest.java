package com.demonstration.microservice;

import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FunctionalFibonacciServiceTest {

    // Functional test to be ran not at build time.
    @Ignore
    @Test
    public void functionalFibonacci() throws Exception {
        // just good path tests for now...
        assertEquals("[0,1,1,2,3,5]", callGetRequest("/fibonacci/5"));
        assertEquals("[0,1,1,2]", callGetRequest("/fibonacci/3"));
        assertEquals("[0,1,1,2,3,5,8]", callGetRequest("/fibonacci/6"));
        assertEquals("[0]", callGetRequest("/fibonacci/0"));
        assertEquals("[0,1]", callGetRequest("/fibonacci/1"));
        assertEquals("[0,1,1]", callGetRequest("/fibonacci/2"));
        assertTrue(testExpectedStatus("/fibonacci/-2", 500));
        assertTrue(testExpectedStatus("/fibonacci/-10", 500));
        assertTrue(testExpectedStatus("/fibonacci/a", 400));
    }

    private static String callGetRequest(String route) throws Exception {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(new HttpGet("http://localhost:8080"+route));
        ResponseHandler<String> handler = new BasicResponseHandler();
        String buff = handler.handleResponse(response);
        client.close();
        return buff;
    }

    private static boolean testExpectedStatus(String route, int status) throws Exception {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(new HttpGet("http://localhost:8080"+route));
        boolean result = response.getStatusLine().getStatusCode() == status;
        client.close();
        return result;
    }

}
