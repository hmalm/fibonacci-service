# fibonacci-service

First Springboot service.
Testing out springboot with a fibonacci example.

dependencies in order to install and run:

- Gradle build tool.
- Java jdk 1.9+

building the project
```
gradle build
```

starting it from development environment
```
gradle bootRun
```

Fibonacci-service:

This service utilizes the GET method and passes in a parameter of the number of n in the fibonacci sequence.
A response is returned of every resulting n - 1 call to fibonacci. I made the choice to include a 0 call of zero,
as that appears to be modern usage according to wikipedia.

example curl usage:
```
GET /fibonacci/8
```
should return something like this:
```
[0,1,1,2,3,5,8,13,21]
```

